package application;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import java.util.Date;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class VueController {
	
	public Integer Baud = 9600;
	
	ObservableList<Integer> Baudratelist = FXCollections
			.observableArrayList(110, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 31250, 38400, 56000, 57600, 115200);

	@FXML
	private Button searchbutton;
	@FXML
	private Button sendbutton; 
	@FXML
	private Button parambutton; 
	@FXML
	private Button uploadbutton; 
	@FXML
	private TextField hexname;
	@FXML
	private TextField ConnectionStatus;
	@FXML
	private TextField serialInput;
	@FXML
	private TextArea serialMonitor;
	@FXML
	private Rectangle statusColor;
	@FXML
	private ChoiceBox<Integer> baudRate;
	
	// === GLOBAL VARIABLES ===
	String log = "Demarrage: ";
	int compteur;
	FileChooser HEXChooser = new FileChooser();
	public Atmega myAtmega; 
	Avrdude myAvrdude;
	String hexPath;
	
	@FXML
    public void initialize() {
        // Initialization code can go here. 
        // The parameters url and resources can be omitted if they are not needed
		
		
		// === TRY SERIAL CONNECTION OF THE ATMEGA ===
		myAtmega = new Atmega(true);
		myAvrdude = new Avrdude(true);
		try{
			myAtmega.connectAtmega();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		baudRate.getItems().addAll(Baudratelist);
		
		// ===	UPDATE GUI COMPONENTS WITH ATMEGA DEFAULT INFOS	===
		baudRate.setValue(myAtmega.getAtmegaBaudRate());
		ConnectionStatus.setStyle("-fx-text-inner-color: grey;");
		ConnectionStatus.setText("Click on 'SEND' to update status...");
		
    }
	
	@FXML 
    private void launchHEX() {
		myAtmega.setAtmegaBaudRate(baudRate.getValue());
		System.out.println("Here");
		if(hexPath.contentEquals(" ")==false) {
			// === try to execute avrdude and start uploading code ===
			 try
		        { 
				 	// Create an Avrdude instance
				 	myAvrdude.setAtmegaType(myAtmega.getAtmegaType());
				 	myAvrdude.setBaudRate(Integer.toString(myAtmega.getAtmegaBaudRate()));
				 	myAvrdude.setComPort(myAtmega.getComPort());
				 	myAvrdude.setOptions("-D -v -cwiring");
				 	myAvrdude.setTargetFile(hexname.getText());
				 	
		            // Command to create an external process 
		            String command = myAvrdude.getPath()
		            				+" " + myAvrdude.getConf()
		            				+" " + myAvrdude.getOptions()
		            				+" -p" + myAvrdude.getAtmegaType()
		            				+" -P" + myAvrdude.getComPort()
		            				+" -b" + myAvrdude.getBaudRate()
		            				+" -Uflash:w:\"" +myAvrdude.getTargetFile()
		            				+"\":i";
		            			
		            System.out.println("command exec : " + command);
		            // Running the above command 
		            Runtime run  = Runtime.getRuntime(); 
		            Process proc = run.exec(command); 
		        } 
		  
		        catch (IOException e) 
		        { 
		            e.printStackTrace(); 
		        } 
			}else {
				// gray out the button if no file selected
				uploadbutton.setDisable(false);
			}
	}

	
	@FXML 
    public void search() {
		HEXChooser.getExtensionFilters().addAll(new ExtensionFilter("hex files", "*.hex"),new ExtensionFilter("All Files", "*.*"));
		File HEXFile = HEXChooser.showOpenDialog(new Stage());
		hexPath =  HEXFile.getAbsolutePath();
		hexname.setText(hexPath);
		
    }
	
	@FXML 
    private void send() { //saisie text serie
		Timestamp timestamp;
		String serialStrMessage;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		if(myAtmega.isConnected() == false) {
			//we retry the connection
			if(myAtmega.connectAtmega()==-1) {
				ConnectionStatus.setText("NOT CONNECTED : check usb connection or restart app");
			}else {
				ConnectionStatus.setText("CONNECTED : ON PORT " + myAtmega.getComPort() );
			}
		}else {
			// Update connection status
			ConnectionStatus.setText("CONNECTED : ON PORT " + myAtmega.getComPort());
		}
		
			// Add timestamp to the current string
			timestamp = new Timestamp(System.currentTimeMillis());				
			serialStrMessage = timestamp + " | " + serialInput.getText();
			serialMonitor.appendText(serialStrMessage+'\n');
		
		updatecolor(); //test couleur
		
		
    }
	
	@FXML 
    private void  selectBaudrate(){ // Baudrate selection
		System.out.println("oui");
		myAtmega.setAtmegaBaudRate(baudRate.getValue());
	}
	
	public void exitApplication() {
		if(myAtmega.isConnected()) {
			myAtmega.disconnectAtmega();
			
		}
	}
	
	private void updatecolor() {
		if (myAtmega.isConnected()) {
			statusColor.setFill(Color.GREEN);
		}
		else {
			statusColor.setFill(Color.RED);
		}
	}
	
	//Pour aller dans les param�tres avanc�s
	public void Gotoparameters(ActionEvent event) throws IOException {
		Parent parametersParent = FXMLLoader.load(getClass().getResource("parameters.fxml"));
		Scene ParametersScene = new Scene(parametersParent);
		
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(ParametersScene);
		window.show();
	}
	
	
	
	
	
}
