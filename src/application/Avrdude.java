package application;

public class Avrdude {

	// === Variables ===
	private String path;
	private String conf;
	private String options;
	private String targetFile;
	private String comPort;
	private String baudRate;
	private String atmegaType;
	private String programmerid;
	
	
	// === Constructor ===

	public Avrdude(boolean init) {
		// Default avrdude location exe and conf files
		if (init) {
			this.setPath("C:\\Program Files (x86)\\Arduino\\hardware\\tools\\avr\\bin\\avrdude.exe");
			this.setConf("C:\\Program Files (x86)\\Arduino\\hardware\\tools\\avr\\bin\\etc\\avrdude.conf");
			this.setTargetFile("UNDEFINED");
			this.setComPort("UNDEFINED");
			this.setBaudRate("UNDEFINED");
			this.setAtmegaType("UNEDFINED");
			this.setProgrammerid("wiring");
		} else {
			this.setPath(getPath());
			this.setConf(getConf());
			this.setTargetFile(getTargetFile());
			this.setComPort(getComPort());
			this.setBaudRate(getBaudRate());
			this.setAtmegaType(getAtmegaType());
			this.setProgrammerid(getProgrammerid());
		}
	}


	// === Methods ===
	
	// === Getters and Setters ===
	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}
	
	public String getConf() {
		return conf;
	}


	public void setConf(String conf) {
		this.conf = conf;
	}
	
	public String getOptions() {
		return options;
	}


	public void setOptions(String options) {
		this.options = options;
	}


	public String getTargetFile() {
		return targetFile;
	}


	public void setTargetFile(String targetFile) {
		this.targetFile = targetFile;
	}


	public String getComPort() {
		return comPort;
	}


	public void setComPort(String comPort) {
		this.comPort = comPort;
	}


	public String getBaudRate() {
		return baudRate;
	}


	public void setBaudRate(String baudRate) {
		this.baudRate = baudRate;
	}


	public String getAtmegaType() {
		return atmegaType;
	}


	public void setAtmegaType(String atmegaType) {
		this.atmegaType = atmegaType;
	}
	
	public String getProgrammerid() {
		return programmerid;
	}


	public void setProgrammerid(String programmerid) {
		this.programmerid = programmerid;
	}

}
