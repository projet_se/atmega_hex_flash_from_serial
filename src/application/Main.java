package application;
	
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
// imports for serial handling 
//import java.io.IOException;
//import com.fazecast.jSerialComm.*;
// USB handling libs


public class Main extends Application {
	

	//static Context context;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			// ===	LOAD MAIN PAGE	===
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Vue.fxml"));
			Parent root = loader.load();
			VueController controller = loader.getController();
			Scene scene = new Scene(root);
			primaryStage.setTitle("HEX uploader+");
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.setOnHidden(e -> {
				controller.exitApplication();
			    Platform.exit();
			});
			primaryStage.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	 public static void main(String[] args) {
	        launch(args);
	 }
	
	 @Override
	 public void stop(){
	     System.out.println("APP : app closed");
	 }
	
	
	
}
