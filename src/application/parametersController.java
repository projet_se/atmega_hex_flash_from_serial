package application;
import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class parametersController {
	FileChooser AVRChooser = new FileChooser();
	
	// Create a tmp atmega object to change atmega related values
	Atmega tmpAtmega;
	Avrdude myAvrdude;
	
	String avrpath;
	String DefaultMicrocontroller;
	String DefaultcomPort;
	String DefaultAVRpath;
	String DefaultProgrammerID;
	
	@FXML
	private Button goBackButton;
	@FXML
	private Button Save;
	@FXML
	private Button Reset;
	@FXML
	private Button searchAVR;
	@FXML
	private TextField MControllername;
	@FXML
	private TextField Portnumber;
	@FXML
	private TextField AVRpath;
	@FXML
	private TextField Programmerid;
	
	@FXML
    public void initialize() {
		tmpAtmega = new Atmega(false);
		myAvrdude = new Avrdude(false);
		DefaultMicrocontroller = tmpAtmega.getAtmegaType();
		DefaultcomPort = tmpAtmega.getComPort();
		DefaultAVRpath = myAvrdude.getPath();
		DefaultProgrammerID = myAvrdude.getProgrammerid();
		MControllername.setText(DefaultMicrocontroller);
		Portnumber.setText(DefaultcomPort);
		AVRpath.setText(DefaultAVRpath);
		Programmerid.setText(DefaultProgrammerID);
		
	}
	
	public void Gotomainview(ActionEvent event) throws IOException {
		Parent VueParent = FXMLLoader.load(getClass().getResource("Vue.fxml"));
		Scene VueScene = new Scene(VueParent);
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(VueScene);
		window.show();
	}
	
	public void ARVSearch() {
		AVRChooser.getExtensionFilters().addAll(new ExtensionFilter("Executables", "*.exe"));
		File HEXFile = AVRChooser.showOpenDialog(new Stage());
		avrpath = HEXFile.getAbsolutePath();
		AVRpath.setText(avrpath);
	}
	
	public void Reset() {
		MControllername.setText(DefaultMicrocontroller);
		Portnumber.setText(DefaultcomPort);
		AVRpath.setText(DefaultAVRpath);
		Programmerid.setText(DefaultProgrammerID);
	}
	
	public void Register() {
		tmpAtmega.setAtmegaType(MControllername.getText());
		tmpAtmega.setComPort(Portnumber.getText());
		myAvrdude.setPath(AVRpath.getText());
		myAvrdude.setProgrammerid(Programmerid.getText());
	}
}

