package application;

import com.fazecast.jSerialComm.*;


public class Atmega {
	
	// === Variables ===
	private static String atmegaType; //ligne à modifier si le nom du port ne contient pas cette chaine de caractère 
	public static int atmegaBaudRate;
	private int atmegaDataBytes;
	private int atmegaStopBits;
	private int atmegaParityBits;
	private int portIndex;
	public String comPort;
	private boolean isConnected;
	private boolean oldConnectionState;
	private SerialPort[] sp;
	String osName;
	static boolean isMacOs;
	static boolean isWinOs;
	
	// === Constructor ===
	public Atmega(boolean init) {
		osName = System.getProperty("os.name").toLowerCase();
		isMacOs = osName.startsWith("mac os x");
		isWinOs = osName.startsWith("windows");
		//Init atmega config
		if (isMacOs) 
		{
			setAtmegaType("IOUSBHostDevice");
		}else {
			if(isWinOs) {
				setAtmegaType("2560");
			}
		}
		
		if(init) {
			setAtmegaBaudRate(115200);	
		}else {
			setAtmegaBaudRate(getAtmegaBaudRate());
			System.out.println("configured baudrate : " + getAtmegaBaudRate());
		}
		setAtmegaStopBits(1);
		setAtmegaParityBits(1);
		
		setConnected(false);
		
	}

	
	// === Methods ===
	public int searchMegaPortIndex(SerialPort[] sp) {

		for (int i = 0; i < sp.length; i++) {
			if (sp[i].getDescriptivePortName().contains(getAtmegaType())) {
				System.out.println("Port found : " + sp[i].getDescriptivePortName());
				return i;
			}
		}

		//System.out.println("Check USB connection and retry");
		return -1;// cette fonction retourne -1 si le port n'a pas été trouvé
	}
	
	public int connectAtmega() {
		if(isConnected() == false) {
			sp = SerialPort.getCommPorts();
			setPortIndex(searchMegaPortIndex(sp));
			if(getPortIndex()!=-1) {
				getSerialPort(portIndex).setComPortParameters(getAtmegaBaudRate() , getAtmegaDataBytes(), getAtmegaStopBits(), getAtmegaParityBits());
			}
		}
		
		
		//ouverture du port com
		if (portIndex != -1 && getSerialPort(portIndex).openPort()) {
			System.out.println("Atmega : Ready for transfer");
			setConnected(true);
		} else {
			System.out.println("Atmega : Connection failed, check USB connexion and configurations");
			return -1;
		}
		return 0;
	}
	
	public int disconnectAtmega() {
		if (portIndex != -1 && sp[portIndex].closePort()) {
			System.out.println("Atmega : COM port closed successfully");
			
		} else {
			System.out.println("Atmega : failed to close the COM port");
			return -1;
		}
		setConnected(false);
		return 0;
	}
	

	public boolean isConnected() {
		sp = SerialPort.getCommPorts();
		for(int i=0; i<sp.length ; i++) {
			//.out.println("Port found : " + sp[i].getDescriptivePortName());
			if(sp[i].getDescriptivePortName().contains(getAtmegaType())) {
				//System.out.println("Atmega still connected!");
				if(portIndex != -1) {
					//we keep the detected comport in memory
					this.setComPort(sp[i].getDescriptivePortName().substring(19, 23));
					isConnected = true;
				}else {
					isConnected = false;
				}
				
				return isConnected;
			}
		}
		isConnected = false;
		return isConnected;
	}

	
	public int getAtmegaDataBytes() {
		return atmegaDataBytes;
	}



	public void setAtmegaDataBytes(int atmegaDataBytes) {
		this.atmegaDataBytes = atmegaDataBytes;
	}



	public String getAtmegaType() {
		return atmegaType;
	}



	public void setAtmegaType(String atmegaType) {
		Atmega.atmegaType = atmegaType;
	}



	public int getAtmegaBaudRate() {
		return atmegaBaudRate;
	}



	public void setAtmegaBaudRate(int atmegaBaudRate) {
		this.atmegaBaudRate = atmegaBaudRate;
	}



	public int getAtmegaStopBits() {
		return atmegaStopBits;
	}



	public void setAtmegaStopBits(int atmegaStopBits) {
		this.atmegaStopBits = atmegaStopBits;
	}



	public int getAtmegaParityBits() {
		return atmegaParityBits;
	}



	public void setAtmegaParityBits(int atmegaParityBits) {
		this.atmegaParityBits = atmegaParityBits;
	}



	public int getPortIndex() {
		return portIndex;
	}



	public void setPortIndex(int portIndex) {
		this.portIndex = portIndex;
	}
	
	public SerialPort getSerialPort(int spIndex) {
		return this.sp[spIndex];
	}


	

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public String getComPort() {
		return comPort;
	}


	public void setComPort(String comPort) {
		this.comPort = comPort;
	}
}
