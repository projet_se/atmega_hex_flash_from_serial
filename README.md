# HEX flash from serial

Ce projet JAVA permet le téléversement de fichier .HEX vers la mémoire flash d'un microcontrôleur de type ATMEGA 2560. Il permet également de communiquer via la liaison série avec un microcontrôleur de type ATMEGA 2560. 

## Contenu du répertoire

`.settings` Fichier système, pas à modifier

`ext` Ce répertoire contient les éléments externes à inclure dans l'application (images, icônes...)

`lib` Ce répertoire contient toutes les librairies utilisées dans le programme JAVA

`src/application` Ce répertoire contient toutes les classes du programme JAVA

`.classpath` Fichier système, pas à modifier

`.gitignore` Fichier qui permet d'exclure du dépôt GitLab certains fichiers autogénérés lors des compilations

`.project` Fichier système, pas à modifier

## Pour commencer

*Ce programme a été édité et compilé sur Windows. Nous n'assurons pas la possibilité de l'éditer et de le compiler sur d'autres systèmes avec les outils suivants.*

- JAVA installé sur le PC de développement
- JavaFx (framework utilisé pour le développement du GUI)
- Un microcontrôleur ATMEGA2560 pouvant être relié via USB à un PC

### Installation

Pour éditer le code JAVA, vous pouvez suivre les étapes suivantes :

1 - Ouvrir [ECLIPSE IDE Version: 2020-03 (4.15.0)](https://www.eclipse.org/)   

2 - Cliquer sur `File`-> `Open Projects from File System... `

3 - Sélectionner le répertoire   `atmega_hex_flash_from_serial`

4 - Cocher le projet contenu puis cliquer sur `Finish`

## Exécution de tests unitaires 

[PAS DE TEST UNITAIRE IMPLÉMENTÉ POUR L'INSTANT]

### Cas d'usage testés

1 - Ouverture de fichier .HEX

2 - Détection de port COM

## IDE

* [ECLIPSE IDE Version: 2020-03 (4.15.0)](https://www.eclipse.org/) - IDE JAVA

## Auteurs

* **Suzanne DUCROT** - Étudiante Systèmes Embarqués - Polytech LILLE
* **Laurie MALARET** - Étudiante Systèmes Embarqués - Polytech LILLE
* **Axel DEFEZ** - Étudiant Systèmes Embarqués - Polytech LILLE
* **Aviran TETIA** - Étudiant Systèmes Embarqués - Polytech LILLE

Encadrants :

* **Blaise CONRARD** - Enseignant - Polytech LILLE
* **Anne-Lise GEHIN** - Enseignant - Polytech LILLE

## License

Tous droits réservés aux encadrants du projet : Blaise CONRARD et Anne-Lise GEHIN

## Sources 

- Communication série :

  ​	[Vidéo/Tutoriel - Michael Shoeffer](https://mschoeffler.com/2017/12/29/tutorial-serial-connection-between-java-application-and-arduino-uno/)